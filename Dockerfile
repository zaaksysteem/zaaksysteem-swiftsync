# Build this using: docker build -t registry.gitlab.com/mintlab/zaaksysteem-swiftsync .
FROM perl:5.30

COPY cpanfile /tmp

# zaaksysteem-syncer uses awscli to do the actual syncing
RUN    apt-get update \
    && apt-get install -y --no-install-recommends --no-install-suggests python3-pip python3-setuptools \
    && pip3 install --upgrade --disable-pip-version-check --no-cache-dir awscli awscli-plugin-endpoint \
    && apt-get purge -y --auto-remove python3-pip python3-setuptools \
    && apt-get autoremove --purge -yqq \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# Install Perl-level dependencies
RUN cd /tmp && \
    cpanm -n --installdeps . && \
    rm -rf ~/.cpanm && rm -rf /tmp/cpanfile

COPY . /opt/zaaksysteem-sync

RUN  chmod +x /opt/zaaksysteem-sync/bin/*

WORKDIR /opt/zaaksysteem-sync

RUN prove -lv t
