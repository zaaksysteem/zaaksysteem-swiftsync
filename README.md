# SSL Gateway for SSL Offloading

## BUILD

```
docker build -t registry.gitlab.com/zaaksysteem/zaaksysteem-swiftsync .
```

## PUSH

```
docker push registry.gitlab.com/zaaksysteem/zaaksysteem-swiftsync
```

## DEVELOPMENT
```
docker run -ti -v $PWD:/opt/zaaksysteem-swiftsync registry.gitlab.com/zaaksysteem/zaaksysteem-swiftsync bash
```

## Test

```
docker-compose exec postgres bash
/opt/zaaksysteem-sync/inc/set_of_2000.sh
```

```
docker-compose exec redis bash
cat /opt/zaaksysteem-sync/inc/redis_config.txt | redis-cli -x set saas:instance:test.zaaksysteem.nl
echo "sadd saas:instances test.zaaksysteem.nl" | redis-cli 
```

```
docker-compose exec swiftsync bash
bin/zyncer --config etc/zyncer.conf --loglevel debug --wait=10 --instance=test.zaaksysteem.nl

## RUN
```
docker run registry.gitlab.com/zaaksysteem/zaaksysteem-swiftsync
```