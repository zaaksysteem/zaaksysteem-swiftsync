#!/usr/local/bin/perl

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";

use Config::General;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use Pod::Usage;
use POSIX ":sys_wait_h";
use Net::Statsd;

use Zaaksysteem::Syncer;

my ($cfg, $logger) = ({},{});

### main()
###
### Main loop of the program

sub main {
    show_usage()            if $cfg->{help};
    show_usage(undef, 2)    if $cfg->{man};

    $cfg->{wait}            ||= 30;
    $cfg->{loglevel}        ||= 'info';
    $cfg->{concurrent}      ||= 5;
    $cfg->{filesperhost}    ||= 250;
    $cfg->{offset}          ||= 0;

    if ($cfg->{statsdhost}) {
        $Net::Statsd::HOST = $cfg->{statsdhost};
        $Net::Statsd::PORT = 8125;           # Default
    }

    my %logmap = (
        trace   => $TRACE,
        debug   => $DEBUG,
        info    => $INFO,
        warn    => $WARN,
        error   => $ERROR,
        fatal   => $FATAL,
    );

    show_usage("Invalid log level: choose one of 'trace,debug,info,warn,error'") unless $logmap{ lc($cfg->{loglevel}) };

    Log::Log4perl->easy_init($logmap{ lc($cfg->{loglevel}) });
    $logger = Log::Log4perl->get_logger();

    show_usage("Missing configuration file (--config)") unless $cfg->{config};

    my $config = _load_config();
    my $zyncer = _load_zyncer($config);

    my $pids;
    while (1) {
        my @instances   = $zyncer->list_instances();

        if ($cfg->{instance}) {
            @instances      = grep { $_ eq $cfg->{instance} } @instances;
        }

        for my $instance (@instances) {
            ### Lets wait for a child to finish
            if (scalar keys %$pids >= $cfg->{concurrent}) {
                while (scalar keys %$pids >= $cfg->{concurrent}) {
                    _check_running_children($pids);

                    ### Easy...prevent 100% CPU
                    sleep 1;
                }
            } else {
                _check_running_children($pids);
            }

            ### When we start the next run, do not go parallel...
            if (grep { $instance eq $_ } values %$pids) {
                $logger->info(sprintf("A process for %s is still running, skip this run", $instance));
                next;
            }

            ### Fire off child
            $logger->debug(sprintf("Starting child for %s", $instance));
            my $pid = fork();

            if ($pid) {
                ### Parent process, register child
                $pids->{$pid} = $instance;
            } elsif ($pid == 0) {
                ### Child process, do task and exit
                _run_sync_for_instance($zyncer, $instance);
                exit 0;
            } else {
                $logger->error("Could not start child for $instance, somehow");
            }
        }

        $logger->info(sprintf("Sleeping %i seconds before next run", $cfg->{wait}));
        sleep $cfg->{wait};
    }
}

sub _check_running_children {
    my $pids = shift;

    for my $pid (keys %$pids) {
        my $kid = waitpid($pid, WNOHANG);
        if ($kid) {
            ### Child ended, remove from list
            delete $pids->{$pid};
        }
    }
}

### Method: _load_config
###
### Loads the configuration from given configuration file

sub _load_config {
    my $appc            = Config::General->new(
        -ConfigFile => $cfg->{config},
        -ForceArray => 1
    );

    my %parsed_config = $appc->getall;

    return \%parsed_config;
}

### Method: _load_zyncer
###
### Load the zyncer module

sub _load_zyncer {
    my $config = shift;

    return Zaaksysteem::Syncer->new(
        $config->{redis_server}
            ? (redis_server => $config->{redis_server})
            : (),
        $config->{config_location}
            ? (config_location => $config->{config_location})
            : (),
        primary => Zaaksysteem::Syncer::FilestoreDescriptor->new(
            name => $config->{primary_filestore_name},
            type => $config->{primary_filestore_type},
            rclone_name => $config->{primary_filestore_rclone_name},
            rclone_bucket_name => $config->{primary_filestore_rclone_bucket_name} // '',
        ),
        secondary => Zaaksysteem::Syncer::FilestoreDescriptor->new(
            name => $config->{secondary_filestore_name},
            type => $config->{secondary_filestore_type},
            rclone_name => $config->{secondary_filestore_rclone_name},
            rclone_bucket_name => $config->{secondary_filestore_rclone_bucket_name} // '',
        ),
        files_per_instance => $cfg->{filesperhost},
        query_offset => $cfg->{offset},
        statsd_enabled => ($cfg->{statsdhost} ? 1 : 0),
    );
}

### Method: _run_sync_for_instance
###
### Runs the synchronization for child

sub _run_sync_for_instance {
    my $zyncer   = shift;
    my $instance = shift;

    $zyncer->sync_instance($instance);
}

### show_usage()
###
### Shows usage summary

sub show_usage {
    my $message     = shift || '';
    my $verbosity   = shift || 1;

    pod2usage({
        -message => $message,
        -verbose => $verbosity,
    })
}

GetOptions (
    $cfg,
    'config=s',
    'wait=i',
    'loglevel=s',
    'concurrent=i',
    'instance=s',
    'filesperhost=i',
    'offset=i',
    'statsdhost=s',
    'help',
    'man',
    'testsuite'
);

### Prevent running of main loop when testsuite is set
main() unless $cfg->{testsuite};

1;

__END__


=head1 NAME

zyncer.pl - Synchronizes files in batched to secondary Swift stores

=head1 SYNOPSIS

    ./zyncer.pl --config /etc/zaaksysteem/zyncer.conf --loglevel debug --wait 30

=head1 OPTIONS

=over 20

=item B<--config FILE>

Path to configuration file, see C<./etc> for example

=item B<--loglevel LEVEL>

Log level to use, choose from C<trace>,C<debug>,C<info>,C<warn>,C<error>,C<fatal>. Default: info

=item B<--wait NUM>

Wait NUM seconds after each run. Default: 30

=item B<--concurrent NUM>

Amount of parallel processes to run. Default: 5

=item B<--instance NAME>

Limit to the given instance hostname, e.g. C<test.zaaksysteem.net>

=item B<--filesperhost NUM>

Amount of files to transfer per host per time, default: 250

=item B<--offset NUM>

The number to start on. The OFFSET clause in SQL. This way, you could run multiple instances
of this one at once.

=item B<--statsdhost HOSTNAME>

The statsd hostname to send statistics to

=item B<--help>

Summary about zyncer usage.

=item B<--man>

Information about this application

=back

=head1 DESCRIPTION

This module enables syncing files from a primary to secondary swift store

=head1 TESTS

Tests can be found in L<./t>. You can test this module by running

    ./prove -vl t/*

=head1 SEE ALSO

L<Zaaksysteem>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
