requires 'autodie';
requires 'AppConfig';
requires 'Config::General';
requires 'DateTime';
requires 'DBD::Pg';
requires 'Digest::SHA';
requires 'File::Temp';
requires 'Getopt::Long';
requires 'IPC::System::Simple';
requires 'JSON';
requires 'List::MoreUtils';
requires 'LockFile::Simple';
requires 'Log::Log4perl';
requires 'LWP::Protocol::https';
requires 'Moose';
requires 'MooseX::Log::Log4perl';
requires 'namespace::autoclean';
requires 'Perl::Version';
requires 'Pod::Usage';
requires 'Redis';
requires 'Try::Tiny';
requires 'Net::Statsd';

# For testing
requires 'Test::More';
requires 'Test::Exception';
requires 'Test::Compile';
requires 'Mock::Quick';
requires 'Data::UUID';
requires 'String::Random';
requires 'Memory::Usage';
requires 'Test::LeakTrace';
