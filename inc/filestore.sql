--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: filestore; Type: TABLE; Schema: public; Owner: zaaksysteem
--

CREATE TABLE filestore (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    thumbnail_uuid uuid,
    original_name character varying(250) NOT NULL,
    size integer NOT NULL,
    mimetype character varying(160) NOT NULL,
    md5 character varying(100) NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    storage_location text[],
    is_archivable boolean DEFAULT false NOT NULL
);


ALTER TABLE filestore OWNER TO zaaksysteem;

--
-- Name: filestore_id_seq; Type: SEQUENCE; Schema: public; Owner: zaaksysteem
--

CREATE SEQUENCE filestore_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE filestore_id_seq OWNER TO zaaksysteem;

--
-- Name: filestore_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaaksysteem
--

ALTER SEQUENCE filestore_id_seq OWNED BY filestore.id;


--
-- Name: filestore id; Type: DEFAULT; Schema: public; Owner: zaaksysteem
--

ALTER TABLE ONLY filestore ALTER COLUMN id SET DEFAULT nextval('filestore_id_seq'::regclass);

--
-- Name: filestore filestore_pkey; Type: CONSTRAINT; Schema: public; Owner: zaaksysteem
--

ALTER TABLE ONLY filestore
    ADD CONSTRAINT filestore_pkey PRIMARY KEY (id);


--
-- Name: filestore_storage_idx; Type: INDEX; Schema: public; Owner: zaaksysteem
--

CREATE INDEX filestore_storage_idx ON filestore USING gin (storage_location);


--
-- PostgreSQL database dump complete
--

