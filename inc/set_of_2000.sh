#!/bin/bash

tmpfile=$(tempfile)

echo "CREATE EXTENSION pgcrypto;" | psql -U zaaksysteem zaaksysteem
for i in $(seq 1 2000); do
    echo "INSERT INTO filestore (uuid, original_name, size, mimetype, date_created, storage_location, md5) values(gen_random_uuid(), 'test_${i}.txt', 128, 'text/plain', NOW(), '{LocalSwift}', MD5('${i}'));" >> $tmpfile
done

psql -U zaaksysteem -f $tmpfile zaaksysteem

rm $tmpfile