package Zaaksysteem::Syncer;

use autodie qw(system);
use Moose;
use DBI;
use Try::Tiny;
use Net::Statsd;

use Zaaksysteem::Syncer::FileConfig;
use Zaaksysteem::Syncer::RedisConfig;
use Zaaksysteem::Syncer::FilestoreDescriptor;

with 'MooseX::Log::Log4perl'; # we like logging

=head1 NAME

Zaaksysteem::Syncer - Zaaksysteem File syncer

=head1 SYNOPSIS

    use Zaaksysteem::Syncer;

    my $zyncer = Zaaksysteem::Syncer->new(
        redis_server => 'redis:6379'
    );

    # or:

    my $zyncer = Zaaksysteem::Syncer->new(
        config_location => '/etc/zaaksysteem/customer.d'
    );

    $zyncer->sync_instances;

    $zyncer->sync_instance("test1.zaaksysteem.nl");

=head1 DESCRIPTION

An abstraction to get the zaaksystem config from Redis

=head1 ATTRIBUTES

=head2 redis_server

Server and port of redis endpoint

=cut

has 'redis_server' => (
    is          => 'ro',
    isa         => 'Str',
);

has 'config_location' => (
    is          => 'ro',
    isa         => 'Str',
);

=head2 _configreader

An instance of L<Zaaksysteem::Syncer::RedisConfig> or L<Zaaksysteem::Syncer::FileConfig>

=cut

has '_configreader' => (
    is          => 'rw',
    isa         => 'Zaaksysteem::Syncer::RedisConfig|Zaaksysteem::Syncer::FileConfig',
);

=head2 files_per_instance

Sets a LIMIT on the number of files per run. This way, we prevend a new copied instance from taking up all the time

=cut

has 'files_per_instance' => (
    is          => 'ro',
    isa         => 'Num',
    default     => 250
);

=head2 query_offset

When limiting the SQL query, set an OFFSET of query_offset. This way you are able to run multiple syncers at the same time.

=cut

has 'query_offset' => (
    is          => 'ro',
    isa         => 'Num',
);

=head2 statsd_enabled

When limiting the SQL query, set an OFFSET of query_offset. This way you are able to run multiple syncers at the same time.

=cut

has 'statsd_enabled' => (
    is          => 'ro',
    isa         => 'Bool',
);

=head2 primary

Primary file store descriptor

=cut

has 'primary' => (
    is => 'ro',
    isa => 'Zaaksysteem::Syncer::FilestoreDescriptor',
    required => 1,
);

=head2 secondary

Secondary (copy) file store descriptor

=cut

has 'secondary' => (
    is => 'ro',
    isa => 'Zaaksysteem::Syncer::FilestoreDescriptor',
    required => 1,
);

=head1 METHODS

sub

=head2 list_instances

    $zyncer->list_instances;

=cut

sub list_instances {
    my $self    = shift;
    my $host    = shift;

    return $self->_configreader->list_instances;
}

=head2 sync_instance

    $zyncer->sync_instance("test1.zaaksysteem.nl");

=cut

sub sync_instance {
    my $self    = shift;
    my $host    = shift;

    my $config  = $self->_configreader->get_config($host);

    die("Cannot find config for host $host") unless $config;

    my $dbh = $self->_get_db_connection($config);
    my $bucket = $config->{storage_bucket} // $config->{instance_uuid};

    die("Cannot find storage bucket for host $host") unless $bucket;

    $self->_sync_files($dbh, $bucket, $host);
}

=head1 PRIVATE METHODS

=head2 BUILD

Do some setup work on object creation.

=cut

sub BUILD {
    my $self = shift;


    if ($self->redis_server) {
        $self->log->debug("Initializing Redis connection");
        $self->_configreader(
            Zaaksysteem::Syncer::RedisConfig->new(
                server => $self->redis_server
            )
        );
    } else {
        $self->log->debug("Using plain files for configuration");
        $self->_configreader(
            Zaaksysteem::Syncer::FileConfig->new(
                config_location => $self->config_location
            )
        );
    }
}

=head2 _get_db_connection

=cut

sub _get_db_connection {
    my $self    = shift;
    my $config  = shift;

    die("Cannot find config for db in given config") unless $config->{'Model::DB'}->{connect_info};

    my $dbconfig = $config->{'Model::DB'}->{connect_info};

    $self->log->debug("Connecting to database " . $dbconfig->{dsn});

    return DBI->connect(
        $dbconfig->{dsn},
        ($dbconfig->{username} || 'zaaksysteem'),
        $dbconfig->{password},
        {
            RaiseError => 1,
            AutoCommit => 1,
        }
    );
}

=head2 _sync_files

    $self->_sync_files($dbh, $bucket);

Syncs files on given database connection and bucket

=cut

sub _sync_files {
    my $self = shift;
    my $dbh = shift;
    my $bucket = shift;
    my $host = shift;

    my $primary = $self->primary;
    my $secondary = $self->secondary;

    my $statement = "SELECT id, uuid, storage_location FROM filestore WHERE "
        . 'storage_location @> ? AND '
        . ' NOT storage_location @> ?'
        . ' ORDER BY id DESC'
        . ' LIMIT ' . $self->files_per_instance
        . ($self->query_offset ? ' OFFSET ' . $self->query_offset : '');

    $self->log->info(
        sprintf("Transferring untransferred files for instance %s", $host)
    );

    my $sth = $dbh->prepare($statement);

    my $rv = try {
        $sth->execute('{' . $primary->name . '}', '{' . $secondary->name . '}');
    } catch {
        $self->log->error(
            sprintf(
                "Host %s: failed executing select on filestore table: %s",
                $host,
                $_
            )
        );

        return;
    };

    return unless $rv;

    my ($success_count, $error_count);
    while (my $row = $sth->fetchrow_hashref) {
        $self->log->debug(sprintf("Transferring file with ID: %d", $row->{id}));

        my $ok = try {
            $self->_sync_file(
                $bucket,
                $row->{uuid},
                $primary,
                $secondary,
            );
        } catch {
            $self->log->error(
                sprintf(
                    "Failed transfering %s/%s to %s (host/filestore_id: %s/%d): %s",
                    $bucket,
                    $row->{uuid},
                    $secondary->name,
                    $host,
                    $row->{id},
                    $_
                )
            );

            return;
        };

        if ($ok) {
            $self->log->debug("Copy completed successfully. Updating database.");
            my $u_statement = sprintf(
                "UPDATE filestore"
                ." SET storage_location = storage_location || '{%s}'"
                ." WHERE id = ?",
                $secondary->name
             );

            if ($dbh->do($u_statement, undef, $row->{id})) {
                $success_count->{$secondary->name}++;
            } else {
                $error_count->{$secondary->name}++;
            }
        } else {
            $error_count->{$secondary->name}++;
        }
    }

    $self->log->info(
        sprintf(
            "Report for '%s' from '%s' to '%s': %d TRANSFERRED, %d ERRORS",
            $host,
            $primary->name,
            $secondary->name,
            ($success_count->{$secondary->name} || 0),
            ($error_count->{$secondary->name} || 0)
        )
    );

    $self->_generate_statistics($dbh, $bucket, $host, $success_count->{$secondary->name});

    my $total_success = ($success_count->{$secondary->name} || 0);

    return $total_success;
}

sub _generate_statistics {
    my ($self, $dbh, $bucket, $host, $success) = @_;

    my $primary = $self->primary;
    my $secondary = $self->secondary;

    my $statement = "SELECT count(id) FROM filestore WHERE "
        . 'storage_location @> ? AND '
        . ' NOT storage_location @> ?';

    $self->log->info(
        sprintf("Transferring untransferred files for instance %s", $host)
    );

    my $sth = $dbh->prepare($statement);

    my $rv = try {
        $sth->execute('{' . $primary->name . '}', '{' . $secondary->name . '}');
    } catch {
        $self->log->error(
            sprintf(
                "Host %s: failed executing select on filestore table: %s",
                $host,
                $_
            )
        );

        return;
    };

    my $count = 0;
    while (my $row = $sth->fetchrow_hashref) {
        $count = $row->{count};
    }

    if ($self->statsd_enabled) {
        $self->log->debug(sprintf("Sending statistics to statsd for host %s and bucket %s", $host, $bucket));
        Net::Statsd::increment("zaaksysteem_sync_all_unsynced_files_count", $count);
        Net::Statsd::increment("zaaksysteem_sync_all_synced_files_count", $success);
    }

    $self->log->info(sprintf("Total unsynced files for instance %s: %d", $host, $count));
}

sub _sync_file {
    my $self = shift;
    my $bucket = shift;
    my $uuid = shift;
    my $primary = shift;
    my $secondary = shift;

    my @command_line = (
        "/opt/zaaksysteem-sync/bin/fcopy",
        $primary->build_rclone_name($bucket, $uuid),
        $secondary->build_rclone_name($bucket, $uuid),
    );

    $self->log->trace("Executing: " . join(" ", @command_line));

    system(@command_line);

    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 TESTS

Tests can be found in L<./t>. You can test this module by running

    ./prove -vl t/*

=head1 SEE ALSO

L<Zaaksysteem>

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
