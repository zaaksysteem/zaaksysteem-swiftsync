package Zaaksysteem::Syncer::ConfigReader;

use Moose;
use Config::General;


with 'MooseX::Log::Log4perl'; # we like logging

=head1 NAME

Zaaksysteem::Syncer::Redis - Connect with Redis

=cut

sub parse_config { 
    my $self = shift;
    my $instance = shift;

    die("Cannot get config from unset instance") unless $instance;

    my $appc = Config::General->new(
        -String => $instance,
        -ForceArray => 1
    );

    my %parsed_config = $appc->getall;

    if (keys(%parsed_config) != 1) {
        die(
            sprintf(
                "Not exactly one instance found in configuration data. Keys found: (%s)",
                join(", ", keys %parsed_config)
            )
        );
    }

    my ($instance_key) = keys %parsed_config;

    return $parsed_config{$instance_key} if $parsed_config{$instance_key};
    return;
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
