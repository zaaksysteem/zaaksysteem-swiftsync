package Zaaksysteem::Syncer::FileConfig;

use Moose;

extends 'Zaaksysteem::Syncer::ConfigReader';
with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Syncer::FileConfig - File-based config reader

=cut

has config_location => (
    is => 'ro',
    required => 1,
    isa => 'Str',
);

sub list_instances {
    my $self = shift;

    opendir my $dir, $self->config_location
        or die "Cannot open config directory: $!";

    my @files = grep {
        -f $_
    } map {
        $self->config_location . "/$_"
    } readdir $dir;

    closedir $dir;

    return sort @files;
}

sub get_config {
    my $self = shift;
    my $instance = shift;

    # Slurp
    open my $fh, "<", $instance;
    my $instance_config = do { local $/; <$fh> };
    close $fh;

    return $self->parse_config($instance_config);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
