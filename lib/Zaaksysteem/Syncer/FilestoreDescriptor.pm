package Zaaksysteem::Syncer::FilestoreDescriptor;
use Moose;

has name => (
    is => 'ro',
    isa => 'Str',
    required => 1,
);

has type => (
    is => 'ro',
    isa => 'Str',
    required => 1,
);

has rclone_name => (
    is => 'ro',
    isa => 'Str',
    required => 1,
);

has rclone_bucket_name => (
    is => 'ro',
    isa => 'Str',
    required => 1,
);

sub build_rclone_name {
    my $self = shift;
    my $dest = join "/", @_;

    if ($self->type eq 's3') {
        return sprintf(
            "%s:%s/%s",
            $self->rclone_name,
            $self->rclone_bucket_name,
            $dest,
        );
    } elsif ($self->type eq 'swift-prefixed') {
        return sprintf(
            "%s:zs-%s",
            $self->rclone_name,
            $dest,
        );
    } elsif ($self->type eq 'swift') {
        return sprintf(
            "%s:%s",
            $self->rclone_name,
            $dest,
        );
    }

    die "Unknown file store type";
}

__PACKAGE__->meta->make_immutable();

1;

__END__

=head1 TESTS

Tests can be found in L<./t>. You can test this module by running

    ./prove -vl t/*

=head1 SEE ALSO

L<Zaaksysteem>

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
