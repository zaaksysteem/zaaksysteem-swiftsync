package Zaaksysteem::Syncer::RedisConfig;

use Moose;
use Redis;
use Config::General;

use constant CONFIG_LIST => 'saas:instances';

extends 'Zaaksysteem::Syncer::ConfigReader';
with 'MooseX::Log::Log4perl'; # we like logging

=head1 NAME

Zaaksysteem::Syncer::Redis - Connect with Redis

=head1 SYNOPSIS

    use Zaaksysteem::Syncer::Redis;

    my $redis = Zaaksysteem::Syncer::RedisConfig->new(
        server => 'redis:6379'
    );

    my @config = $redis->list_configs;

=head1 DESCRIPTION

An abstraction to get the zaaksystem config from Redis

=head1 ATTRIBUTES

=head2 _redis

A L<Redis> object.

=cut

has _redis => (
    is       => 'rw',
    isa      => 'Maybe[Redis]',
    lazy     => 1,
    builder  => '_build_redis',
);

=head2 server

A servername

=cut

has server => (
    isa      => 'Maybe[Str]',
    is       => 'ro',
    required => 1,
);

=head2 _build_redis

    $self->_build_redis

=cut

sub _build_redis {
    my $self    = shift;

    return unless $self->server;

    my $redis   = Redis->new(
        server      => $self->server,
        reconnect   => 30,
    );

    return $redis;
}

=head2 list_instances

    my @instances = $self->list_instances

=cut

sub list_instances {
    my $self            = shift;

    my @members         = $self->_redis->smembers(CONFIG_LIST);

    return sort @members;
}

=head2 get_config


=cut

sub get_config {
    my $self            = shift;
    my $hostname        = shift;

    my $instance        = $self->_redis->get('saas:instance:' . $hostname);

    unless ($instance) {
        $self->log->error("No instance found by hostname: " . $hostname);
        return;
    }

    return $self->parse_config($instance);
}

=head1 PRIVATE METHODS

=head2 BUILD

Constructor: connect to redis

=cut

sub BUILD {
    my $self    = shift;

    $self->_redis;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 TESTS

Tests can be found in L<./t>. You can test this module by running

    ./prove -vl t/*

=head1 SEE ALSO

L<Zaaksysteem>

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
