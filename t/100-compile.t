use Test::Compile;
 
# The OO way (recommended)
my $test = Test::Compile->new();
$test->all_files_ok();
$test->done_testing();
