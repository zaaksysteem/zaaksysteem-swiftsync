use Test::More;
use Test::Exception;
use Test::Moose;
use Mock::Quick;

# Enable logs
use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($TRACE) if $ENV{TEST_LOG};

my $PACKAGE = "Zaaksysteem::Syncer::RedisConfig";

use_ok($PACKAGE);

can_ok($PACKAGE, qw/list_instances get_config/);
has_attribute_ok($PACKAGE, $_, "Public attribute '$_' found") for qw/
    _redis
    server
/;

throws_ok(
    sub {
        my $redis = $PACKAGE->new();
    },
    qr/Attribute \(server\) is required/,
    "Exception check: missing servername"
);

done_testing();